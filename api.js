var logoSRC  ='';
  if (c4rRef['logoSRC']) {
     logoSRC = c4rRef['logoSRC'];
   }
  $('body').before('<style>#c4r-overlay{position: absolute;display:none;top: 0;left: 0;width: 100%;height:100%;z-index: 10;background-color: rgba(0,0,0,0.5); /*dim the background*/}</style><div id="c4r-overlay" onclick="overlayOff()"></div><div id="c4r-altOverlay" onclick="opencars()" style="font-family:arial;z-index:999;left:50%;top:50%;transform:translate(-50%,-50%);position:absolute;color:white;text-align:center;display:none;"><img src="'+logoSRC+'" ><p>Click here to open window again</p></div>');
  $('#ref-api-box').append('<button id="apiButton" onclick="opencars()" style="background:red;cursor:pointer;color:white;font-size:20px;padding:30px;'+c4rRef['apiStyle']+'">'+c4rRef['apiButton']+'</button>');
function opencars(){
   document.getElementById("c4r-overlay").style.display = "block";
   document.getElementById("c4r-altOverlay").style.display = "block";
  var styleSource  ='';
    if (c4rRef['styleSRC']) {
       styleSource = "?style="+c4rRef['styleSRC'];
     }
     //instead of "http://ref.cars4rent.ge/vehicle/" write your URL
     //overlayName is required to close window before reopen it...
  PopupCenter("http://ref.cars4rent.ge/vehicle/"+c4rRef['apiKey']+styleSource, "c4rOverlayName", "90", "70");
}
function overlayOff(){
  document.getElementById("c4r-overlay").style.display="none";
  document.getElementById("c4r-altOverlay").style.display="none";
}

//find screen resolution and resize browser window...

function PopupCenter(url, title, w, h) {  
	if (window.screen) {
        w = window.screen.availWidth * w / 100;
        h = window.screen.availHeight * h / 100;
    }
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
}